package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	const insultURL string = "https://evilinsult.com/generate_insult.php"

	resp, err := http.Get(insultURL)
	if err != nil {
		fmt.Println("FATAL: Cannot get insult.")
		os.Exit(1)
	}
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))

}
